# DiNet jQuery Client

This is a client for chatting on the [Diluvian Network](https://bitbucket.org/scassidy/dinet). To use, download these files to your computer and open client.html.

## Using this client

Everyone you want to speak with should share two pieces of information: a hexadecimal prefix (such as 0fcabe or 01) that is always an even number of digits, and a password. Type these into the client.

Then, set the username you wish to use, and a dinet HTTP server. It does not matter if all parties are using the same HTTP server as long as they are in the same network.

Then press Submit to send messages. Have fun chatting!

